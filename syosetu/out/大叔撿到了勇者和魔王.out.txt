おっさん、勇者と魔王を拾う
チョコカレー
http://ncode.syosetu.com/n8756en/

冒険者ギルドを退職させられたおっさんは故郷に帰る事にする。
その道中、彼は二人の赤子を拾う。一人目は勇者の紋章を宿した人間の赤子。二人目は魔王の紋章を宿した魔族の赤子であった。
かくして、勇者と魔王を拾ったおっさんの物語が幕を開ける。
【書籍化決定】２０１８年７月１０日よりTOブックス様より書籍化させて頂く事になりました。

－－－－－－－－－－－－－－－
其他名稱：
大叔撿到了勇者和魔王

－－－－－－－－－－－－－－－
貢獻者：史卡辛米之刃、蝶舞凝汐

標籤：R15、node-novel、syosetu、おっさん、オリジナル戦記、勇者、勘違い、残酷な描写あり、異世界、魔王

－－－－－－－－－－－－－－－
目錄索引：
- null
- - １：兩個嬰兒
- - ２：村での生活
－－－－－－－－－－－－－－－

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝START


＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝CHECK
第00001章：null
null
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

－－－－－－－－－－－－－－－BEGIN
第00002話：１：兩個嬰兒
１：兩個嬰兒
－－－－－－－－－－－－－－－BODY

第一节两个婴儿
　　所谓的冒险者公会，就是接受委托、进行处理的组织。而冒险者则是只要测试合格的話无论是谁都可以当上的简单职业，能够挑选委托的話就可以减少风险完成任务，还可以赚零花钱，正是小孩子们所憧憬的职业。
　　但是，世间万物皆有寿命，通常来说，虽然冒险者谁都可以当，但这只是表象，冒险者中，也存在着不适合的人和能力不够的人。而在这之中，有一种是因为寿命问题而当不了的。
　　冒险者以十多岁和二十多岁的人居多，毕竟这时是人类的实力巅峰期。当然，三十多岁还是冒险者的人也有，四十、五十岁了还没有衰老的强者也存在，但是这些人只是一小部分。大多数因为年龄问题而无法继续从事工作的人，会由公会發表战力外通告。
　　「十分抱歉，亚莲先生。我们认为您已经无法再承受冒险者工作的重担了。」

　　坐在前台的公会职员一脸为难地说道。听到此話的一脸鬍鬚的男性——亚莲也露出了苦闷的表情。
　　快四十岁的冒险者大多数会自行离开冒险者公会，但也有人因为發布战力外通告才离开。但是，这样的人，通常都是因为憧憬冒险者的身份，觉得将冒险者作为副业对於普通生活也没有大碍的一般人。但问题是，亚莲并不是冒险者中的「一般人」。

　　「是吗……」

　　「亚莲先生在本公会十分活跃干得很棒……但毕竟年龄到了，体力也衰退了，听说在上次事件现场还發生了苦战。因此我们判断，继续进行冒险者活动对您并不好。」

　　现在的亚莲已经快四十岁了，即使仍然想要继续当冒险者，但体力也在日渐下降，如果继续接受委托的話很有可能会死，公会高层也不想發生这种事情吧。正因为冒险者时常陷入生死危机，所以要尽力不让冒险者在事件现场死亡的状况出现，考虑到周边的印象不如说这种事情非常敏感。因此，作为高龄冒险者的阿伦要是死掉的話，公会会很困扰的。
　　公会人员再给亚莲发了战力外通告之後就等待他的回答，他的脸上露出十分复杂的表情，连注意到这一点的亚莲都难过了起来。
　　亚莲知道要回答什麼，要是继续任性下去的話，确实可能因为委托而死，在自己拉起来的队伍裡也成为了拖後腿的存在，这时候正是退隐之时。
　　下了这样的判断之後，亚莲朝工作人员低下了头，并感谢公会一直以来的照顾。
　　就算离开了冒险者公会也是没有退休金的。
　　毕竟，本来冒险者公会这种东西就不是正式的组织，究其根本，也是因为在过去存在魔王的时代，感到战力不足的国家为了非正式雇佣而临时创建的组织。虽然现在还留有公会之名，但继续当冒险者的人就是志愿者了，当然没有退休工资。
　　「啊咧～？这不是亚莲先生吗？」

　　对面一位领导模样的青年注意到亚莲的情况後，也过来搭話了。
　　经过锻炼的肌肉、很高的身高、从体内能感觉到魔力，年轻人真棒啊，看到他之後，亚莲陷入了伤感之中。
　　「这个样子，难道说已经辞退了公会吗？」

　　「啊啊，一直以来的照顾，谢了。」

　　「呀～确实亚莲先生最近在队伍裡面一直在拖後腿，看上去很恶心诶，辞退难道不是贤明之举吗？」

　　青年直截了当的说道。就算不这麼直接也可以的吧？亚莲感觉自己被打脸了，果然这就是现实的痛感啊。
　　他们是这周边有名的队伍，亚莲曾在他们还是雏鸟的时候为了照顾一下而进入他们的队伍。那个时候，阿伦就看到过他们让人眼花缭乱的活跃着。随後亚莲明悟了，啊啊，这就是时代的交替啊。
　　「不过不用担心哟。就算亚莲先生走掉了，我们还是会好好干的。」

　　「是啊……你们的話一定没问题的。」

　　这是事实，他们早就不需要自己了，比起我这样的大叔，能组进队伍的其他冒险者比比皆是，甚至只有他们都足够了。这样一想，亚莲就再也不担心了，这样就可以不留後患的离开这条街了。
　　「那麼、永别了。请健康的在乡下生活吧。」

　　青年说完就离开了，他们的队伍跟在他身後也要离开了，但是有一位穿着白色法袍的少女站在亚莲的面前看着他，这好像是见习魔导士，过去亚莲曾经教过很多关於魔法的事情给她。这位少女蜷缩着肩膀朝亚莲低下了头。
　　「那个……至今为止，承蒙关照了。」

　　她说完之後就慌慌张张地往青年那儿追过去了。
　　作为这个时代的孩子，真是很有礼貌了。在队伍裡面，她也是一个十分老实的孩子。那个孩子，今後会成为大魔导师的吧。思考着这种事的亚莲一想到自己果然是因为成了大叔才有这种想法，顿时苦笑起来。
　　「行了……走吧。」

　　亚莲背上行李，这一次，就不再是出去冒险而离开这条街了。
　　回故乡的途中要使用马车，要是徒步的話怎麼说也要幾个星期，途中又休息了幾次，最後的路则决定走过去。幸好没碰上盗贼、魔物的袭击，马车顺利的到达了。然後亚莲走在令人怀念的山路上，朝村子裡走去。
　　「呼、这周边的路好怀念啊……以前一出村子就在森林裡好好玩了一通。」

　　不知不觉开始回忆起过去的事情了，那个年代真是太好了，完全没注意到进了森林就迷路的自己放肆的玩着。但最重要的是那时候体力真是充沛啊，那个时候，不管再怎麼玩都不会累，而如今，只是潜入地下城都会气喘吁吁。亚莲一边怀念一边走着。
　　「干了多少年呢……」

　　背着行李，大口深呼吸的亚莲喃喃道。
　　离开村子的时候，自己还是青年，那个时候自己只想着不能一辈子在这个乡下终了一生，所以赶紧离开了村子，而现在一心却只想着赶紧回到村子，人的一生真是不可思议，明明还是小孩子的时候，只想着往前走往前走，但现在却只想找个安静的地方过完餘生。
　　亚莲沉浸在各种想法之中，突然间感受到一股奇怪的感觉，於是他停下了脚步。
　　「……嗯？」

　　不是魔物的气息，是连野兽的气息都不如的十分弱小的气息。感受到这股气息的亚莲警戒地放下行李，从腰间拔出剑来。
　　虽然从冒险者公会离开了，但是亚莲也不是弱小到会被魔物随便打倒的程度。不如说亚莲自信自己比那裡的冒险者更强。但问题是体力不足，年龄也让他无法进行长时间的战鬥了。
　　因为公会的委托大多数都是担任重要人物的护卫工作或者是探索地下城这种长时间的任务，因此才会以这种理由辞退亚莲。但是如果只是幾只魔物那还是可以轻鬆干掉的。他一边警戒着，一边朝發出气息的地方走去。不一会儿，就找到在一棵树的下面放置着的笼子。他朝笼子慢慢走近，却發现笼子裡面放着婴儿。
　　「婴儿……？为什麼会在这种森林裡面……？」

　　才出生幾个月的小小的婴儿躺在笼子裡，没想过会是这个情况的亚莲不由自主抱起了头。
　　为啥会有婴儿放在这个森林裡面啊？是丢弃的小孩吗？就算如此这条路也只是通往故乡的路，行人也好商人也好都不经过的，换言之不应该有人特地为了丢小孩跑到这条路上的。但是不仅特地準备了笼子，还认真的盖上了布，而且清楚的留下了人行走的痕迹。有点奇怪，亚莲皱着眉头想到。
　　「啊—……啊——」

　　「哦哦，乖哦乖哦。」

　　突然，婴儿和他视线相交，然後就叫了起来。亚莲慌慌张张地跑到婴儿身边，一脸微笑说着各种安慰的話。为啥我要做这种事情呢……？虽说以前曾经关照过很多冒险者，但是照顾婴儿啥的还真是第一次做。阿伦一边困惑不已，一边拼命哄着婴儿。
　　「总之先带到村子裡去吧……呆在这儿说不定什麼时候就会有魔物袭击……」

　　亚莲嘟囔着，决定首先将婴儿带到村子裡去。说不定村子裡会有人愿意照顾呢。还是有很多疑问，比如说为什麼会把婴儿放到这个森林裡面，这个地方虽然魔物不堪一击，但是对婴儿而言还是十分危险的。对孩子没有爱才这样做吗……但这样的話还特地使用笼子就有点奇怪了……果然还是不懂。亚莲歪着脑袋思考着这些问题的时候，突然注意到，在婴儿的手背上，有什麼印记一样的东西。
　　「嗯？这是什麼……」

　　亚莲抬起婴儿的小手仔细看了起来，幸好婴儿似乎并不怕亚莲，所以没有哭闹，因此亚莲十分简单的观察婴儿的手，随後确认在婴儿的手上确实有个印记，这是一个剑一样的独特印记，亚莲感觉自己好像在哪儿见过这个，歪着脑袋思考起来。
　　「呼姆……总感觉，有点像《勇者的纹章》……应该是偶然吧。」

　　亚莲想起来自己在哪儿看到了。
　　没错，这是在自己读历史书的时候看到的勇者的徽章。那是传说中，唯一能够对抗魔王，生来持有特殊能力的被称为被选中的孩子的人。这个纹章表示的是勇者的家族，但是这个家族已经不在了。最後勇者和魔王同归於尽，世界恢復了和平。此後就再也没有出现过勇者和魔王了，这两人已经成为了最古老的传说。所以这个印记应该就是个普通的印记吧，亚莲只能如此解释。
　　「这麼说来，似乎魔王也有个类似的纹章，好像是这边是个翅膀的纹章……」

　　亚莲又想到了歷史上记载的关於魔王纹章的事。魔王似乎是和勇者性质完全相反的存在，因此魔王身上也有魔王一族的纹章，只不过他是魔王那一族之中具有压倒性的强大魔力的存在。
　　「……嗯？」

　　就在亚莲考虑这些事情的时候，他再次感受到了奇妙的气息。这股气息具有强大的魔力，由於魔物也有魔力，因此说不定是敌人，亚莲想到，他再次拿起剑，抱着小婴儿朝气息方向而去，而在那个地方的是——
　　「……喂喂，又来啊？」

　　这一次放置笼子的地方换成了岩石背面，亚莲心想着「不会吧」然後慢慢靠近确认情况，果然裡面也有个婴儿。居然会捡到两个婴儿，这什麼世道啊！亚莲头脑一片混乱。因为一直在公会裡接任务，所以从来没打听过世界上的信息，难道说现在小孩子身上正流行着什麼瘟疫吗？我已经搞不明白了啦——亚莲内心叹息道。
　　「啊……啊——」

　　「怎麼会有两个婴儿在森林裡……哈啊、总之先带到村子裡吧……嗯？」

　　亚莲摇摇头，没有办法，还是负起责任把这两个婴儿带到村子吧。他一边想着，一边将手伸向笼子裡的婴儿，然後他就注意到在这个婴儿的手背上也有一个印记，翅膀一样的印记……但是，虽然亚莲注意到这个印记很像《魔王的纹章》，但还是觉得应该只是巧合。
　　（……好迟钝的男主……）

　　「和《魔王的纹章》类似啊……这种奇怪的偶然也是有的呢……」

　　亚莲没有过多在意这个，就这样抱着两个婴儿回到了村子。路上亚莲意外的感觉婴儿好重，他一边喘气一边走着。就这样终於回到了想念已久的村子，数年未归的亚莲抱着两个婴儿归来的事情顿时在村子裡引發了巨大的骚动。



－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00003話：２：村での生活
２：村での生活
－－－－－－－－－－－－－－－BODY




　数年振りにアレンが戻った村は昔と何も変わっていなかった。元々辺境の山の中にある村だから外界との接触も少ない為、独自の文化だけで形成されたその場所が大きな変化を迎える事は早々無いのだ。という訳で彼もまた昔使っていた家にまた住む事になった。村人達も久しぶりにアレンが帰って来た事に喜んだ。皆アレンと同じく歳をとっていたけれども。爺さんだった村長がまだ生きていたのはアレンもビックリだった。と言う訳でアレンはまた昔のように山の中で生活を始める事になったのだが……昔とはちょっとだけ違う部分があった。

「父さん！　もう一本！」
「おいおい、まだやるのか……？」

　ブロンドの髪を長く伸ばして後ろでポニーテールで纏め、綺麗な金色の瞳をした少女。長いまつ毛に幼いながらも容姿はパーツの一つ一つが整っており、将来美人になるであろう事が予想される。彼女の名はリーシャ。アレンが最初に拾った赤ん坊。今はアレンの娘という事で育てており、村人達も本当にアレンの娘だと思っている。というかアレンが村に戻った時あまりの騒ぎになってしまい、否定する暇もなく結局流れで彼が育てる事になってしまったのだ。

　そんな訳でリーシャ達の暮らしが始まり早八年。八歳になったリーシャはどういう訳か剣術に目覚め、アレンに教えを乞うようになっていた。庭で剣の稽古をしていたのを偶然見られて以来、彼女は剣にハマり始めたのだ。最初は子供が単純に遊びたいだけなのかと思ってアレンは軽く教えていたが、これが驚くべき事にリーシャは凄まじい程に剣の才能を持っていた。僅か八歳にも関わらずアレンに迫ってくる剣技を見せてくる。単純に自分が老いで弱くなっているだけかも知れないが。

「少しは休憩しよう、リーシャ。俺はもう疲れたよ……」
「やだ！　父さんに勝つまでやる！！」

　それからと言うものアレン達は毎日庭で剣の打ち合いをしているのだが、元気が有り余っているリーシャは何度もアレンに挑戦して来た。身長や体格の差などものともせず、鋭い剣技にアレンを倒そうとしてくるのだ。そうなると彼も流石に八歳の子供に負ける訳にはいかない為、手を抜かずにきちんと戦う。すると当然アレンが勝つ訳なのだが、リーシャは悔しがって何度も挑戦して来るのだ。やはり勇者の紋章っぽいアザを持ってるだけあって才能は凄まじいなとアレンは思う。

「って言ってももう二十戦はしてるぞ？俺はもう脚がパンパンで動けん」
「ん～……分かった。その代わり明日も特訓してよ？」
「ああ、分かってる。約束だ」

　流石に二十戦連続でするとアレンの体力も限界が近かった。ただでさえギルドを辞める頃に体力の衰えを感じ、それから八年も経ってるのだ。子供相手でも疲れてしまうのだから、おっさんだという事が痛感させられる。
　アレンはその場にゴロンと腰を下ろした。脚の疲れが一気に広がってくる。リーシャは疲れていないのだろうかと思って見てみると、彼女は笑いながらアレンと同じ目線で見つめて来ていた。

「はぁ～……やれやれ、リーシャは疲れてないのかい？」
「全然！　剣を振るの楽しいし、面白いもん」
「そうかい……リーシャは将来きっと剣の達人になるだろうね」
「うん！　いつか父さんを超えるんだから！」

　リーシャは剣が大好きだ。だからきっと素晴らしい剣士になる。アレンがそう伝えると彼女は満面の笑みでそう返して来た。きっと自分の事などすぐに超えてしまうだろう。後数年もすれば冒険者にだってなれる。リーシャにはそれくらいの才能があった。少し羨ましいなとアレンは思う。子供の頃からこれだけの才能を発揮しているのは正に天才と称すべき存在だ。ただ同時に自分の事のように嬉しいとも思える。今はリーシャ達の成長を見ているのがアレンにとって一番の幸せなのだ。

「リーシャならすぐ俺を超える事が出来るよ」
「無理だよー。だって父さん、剣も上手いし、魔法も使えるじゃん！　私はルナみたいに上手に魔法使えないもん」

　持っている木剣の剣先を地面に当ててクルクル回しながらリーシャはそう言う。その声は少しだけ羨ましそうな声色が含まれていた。
　確かにリーシャは剣の才能は凄まじいが、魔法の才能はそれ程でもない。というのも使えない訳ではないし、剣の才能があまりにも飛び出ているだけなのだが。妹のルナと比べてしまうと彼女はどうしても劣っていると思ってしまうのだろう。同時に剣と魔法を使えるアレンの事も憧れの対象で見てくる。そんなの王都に行けば両方使える人間など幾らでも居るし、珍しくはない。というかそういう人物は器用貧乏の為、一点特化の方が重宝されるのが現実なんだがなとアレンはこっそり心の中で呟いた。

「俺は冒険者だった頃にちょっと魔法を齧ってたからな。人より長くやってるから程ほどに使えるだけさ」

　アレンが魔法を覚えたのはダンジョンに潜ってる時に剣だけでは限界を感じ、応急處置として魔法を使用していただけだ。もしも〈本物の冒険者〉なら剣だけでも問題なかっただろう。リーシャみたいな剣の才能を持つ者ならそんな限界も難なく突破する。結局は自分は凡人だったという訳だ。アレンはそう考えていた。

（……冒険者、か）

　ふいにアレンは自分のゴツゴツの手を見つめ、握り締めた。
　自分が冒険者だった頃の事がつい最近の事のように思える。冒険者を辞めてからもう八年も経っているのか。時間が経つのは全く早いものだ、と彼は思いながら小さくため息を吐く。
　別に未練がある訳ではない。今の生活には満足だし。娘達の成長を見ているのは楽しい。少なからず冒険者だった頃の自分の技術と知識が役立ってくれているのも嬉しいし。きっとこの子達ならすぐに立派な人間になる。冒険者だって、何だってなれるだろう。アレンはそう感慨深く思った。

「父さん？どうかしたの？」
「ん……ああいや、何でもないよ。ちょっと昔を思い出してただけさ」

　ふとリーシャに心配そうに声を掛けられる。彼女は顔を下げて俺の事を覗き込むような体勢を取っていた。アレンは心配を掛けないよう笑みを浮かべて返事をする。
　ついつい感傷的になってしまった。最近はこればっかりだ。やはり未練がないとか言っておきながら実は未練があるのだろうか？自分の事はよく分からん。アレンは反省するように頭を掻いた。

「昔って、父さんが冒険者だった頃の事？」
「うん……まぁな」
「父さんって凄腕の冒険者だったんでしょ？良いな―、私も冒険者になってみたい」
「凄腕じゃない。長く冒険者をやっていたから少しだけ有名だっただけだ。結局は歳で辞めたんだからな……まぁ、リーシャならすぐなれるよ。冒険者にもなんにでも」

　リーシャ達にも時々自分が冒険者だった頃の事を話している。主にリーシャが聞きたがっているだけなのだが、アレン自身の事はそこまで語らず、冒険者がどういうものなのか、どんな依頼があるのかとかを教えただけだ。リーシャは凄く興味のある顔で聞いていたが、ルナはちょっとだけ怯えていた。あの子はちょっと怖がりだから。

「さ、そろそろ家に戻るぞ。ルナも一人で寂しがってるはずだ」
「うん！　そうだね」

　話をしている内にアレンの体力も戻り、彼は起き上がって家に戻る事にした。リーシャもそれに賛成し、アレン達は家へと戻る為に歩き出した。彼女は少し速足でアレンよりも先を歩く。ブロンドの髪を揺らしながらちょこちょこと先を歩いて行く姿は微笑ましい。リーシャは成長が早い為、背もその内自分を抜くかも知れない。いや、自分は背はそこそこあるからそれはないか。というか娘に見下ろされるような状況はあって欲しくないとアレンは願った。

　村に住み始めてから他に変わった事と言えばアレンはよく読書をするようになった。時には新聞だったりよく分からん古文書だったりとかだが、冒険者のように依頼を受けなくなってからは無性に字が読みたくなったのだ。それ以来アレンは村の人達の本を借りたり、時折やって来る商人から本を売ってもらったりしていた。そのおかげでアレンの寝室はちょっとした図書館になっていた。

「……ふぅん。予言者曰く再びこの世に魔王が現れ、そして希望の星である勇者もまた目覚める、ねぇ……その割には世界は平和だな」

　今日はアレンは椅子に座りながら新聞を読んでいた。王都と違って当然山の中の村では新聞など早々手に入らないが、時折やって来る商人が新聞を持っていたりするのだ。数日前のだったりするが、それでも今は外の情報を知れるのは嬉しい。アレンはそれを譲ってもらい、時折こうして目を通していた。

　そして数週間前の新聞曰く、王都の預言者によって再び世に魔王が目覚めるという不吉な報せがあったらしい。同時に勇者も目覚めるらしいが……中々世界に異変は起こらない。この予言は丁度アレンがリーシャとルナを拾った八年前から言われていた事だが……全く予言というのはアテにならないものだ。何年か前も世界の終わりがやって来るー、とか言うナントカ文明の預言が噂されていたが、結局それも起きなかった。やはり占いは信用出来ないな。アレンは小さくため息を吐きながら新聞を机の上に置いた。

「やれやれ、嘘っぽい話しばかりだな……もっと面白い報せはないのか……ん？」

　大体の本は読んでしまった為今は新しい事が掛かれている新聞を読むのがアレンの趣味なのだが、こうも実際に起きない情報ばかりではつまらない。そんな不満を抱いていると彼は背後から気配を感じ取った。長年冒険者をやっていたから気配を感じ取るのは得意だ。ダンジョンの中ではいつどこから魔物が現れるか分からなかったからである。おかげで背後を人に取られる事はなくなった。ただ、今回は別に警戒する必要は無さそうだ。アレンはそう思いながら顔を後ろにゆっくりと向け、その人物に優しく話し掛けた。

「どうかしたのかい？ルナ」
「……お父さん」

　部屋の扉の所に立っていたのは娘のルナであった。アレンが拾った二人目の赤ん坊。手の甲に〈魔王の紋章〉とよく似た翼のようなアザを持つ少女。
　ルナはリーシャと対照的にとても大人しい子だ。見た目も漆黒のように黒い髪を肩まで伸ばし、同じく真っ黒な瞳をしている。ちょっと垂れ目で弱々しい顔つきをしており、大人しい性格から病弱なイメージがあるが。健康でしっかりとした子だ。どちらかと言うと可愛らしい小動物のような印象と言えるだろう。彼女は扉の前で恥ずかしがるようにもじもじと手を動かしながら俺の上目遣いで視線を向けて来た。

「また、魔法教えて……？」
「ああ、そう言えば約束していたね。分かった、教えてあげよう。おいで」

　ルナもまたリーシャが剣を好きなように彼女の場合は魔法が好きだ。しかもその才能は我流で魔法を覚えたアレンでも分かる程目を見張る物で、恐らく賢者や大魔導士レベルの魔法力であった。単純に言ってしまえば教えた魔法をすぐに理解し、使えるようになるのだ。通常は一つの魔法を覚えるのに長い年月を掛けるものである。初級魔法だってその日に使えるような事はない。魔法とはそういう世界なのだ。なのにルナはその常識を覆すように次々と魔法を習得していった。

「今日は何の魔法が知りたい？」
「治癒魔法……リーシャが怪我した時とかに、使えるようにしときたい」
「ああなるほど、ルナは優しいな」

　部屋に招き入れ、互いに床に座りながらアレン達は向かい合う。そして何を覚えたいかと尋ねると治癒魔法と述べて来た。どうやらいつも特訓で擦り傷ばかりになって帰ってくるリーシャの事が心配だったらしい。確かに女の子が顔に傷を作ったりしたら大変だ。アレンも加減はしているが手を抜く事は出来ず、どうしてもリーシャとの打ち合いでは傷が出来てしまう事がある。彼女は気にしない所かそれを誇りに思っているが、ルナのような反応が一般的な女の子のものだろう。

「治癒魔法は便利だ。対象者の体力を変換させて治癒能力を大幅に底上げし、一瞬で傷を治す事が出来る。俺も冒険者の時はよくお世話になったよ」

　治癒魔法は便利な魔法として重宝されている。通常は覚えるのが中々難しいのだが、逆に覚えてしまえば一気に戦闘が楽になる上、魔法は使えば使う程技術が昇華する事から冒険者みたいな日々傷が絶えない者とは非常に相性が良い。パーティーを組むなら一人はこの治癒魔法を覚えている者が良いと定番になる程だ。アレンも昔はこの鍛え上げた治癒魔法でパーティー内ではそこそこ頼りにされていたんだが……すぐに別の仲間が治癒魔法を習得すると全く御用にされなくなってしまった。

「じゃぁまずは治癒魔法の基礎から教えようか。と言ってもルナならすぐ使えるようになると思うが……」
「よろしくお願いします」

　早速アレンは治癒魔法の授業について始める事にする。ルナなら簡単に習得出来るだろうが、律儀な彼女は深々と頭を下げてから教えを乞うた。本当によく出来た子だ。アレンは頷いてから授業を始めた。

　数分間教えた後、案の定ルナはもう治癒魔法を使いこなす事が出来るようになっていた。試しに枯れた花に試してみたみるみるうちに綺麗な花に戻ったのだ。正直予想以上である。大抵の初心者なら治癒魔法のような高度な魔法は最初は不発に終わるはずなのだが、ルナは一発で成功してみせた。本当に素晴らしい才能持つ子だとアレンは嬉しそうに顔を頷かせた。

「流石だなルナ、効力も完璧だ。治癒魔法を完璧に使いこなしてるよ」
「ううん、お父さんの教え方が上手だからだよ」
「俺の教え方なんで我流だぞ？冒険者だった時にただがむしゃらに使い続けて覚えた魔法だからな」

　ルナは気を遣って自分のおかげだと言ってくれるが、アレンの教え方など殆ど抽象的な物だ。自分の中にある力をこう手先に集めてぐわーっと使うといった、そんな程度の教え方である。正直アレン自身もルナが分かっているのかどうかは分からない。基礎だけは一応きちんと教えているが、ひょっとしたらルナならそれだけでも十分なのかも知れない。まぁ例えどんな教え方でもルナが凄い事は事実なのだ。過程などはどうでも良いのだろうとアレンは勝手に納得した。

「……ねぇ、お父さん」

　そんな事を考えているとふとルナがアレンに話しかけて来た。ハッとなって彼はルナの方を見る。ルナはどこか探り探りのような、こちらを伺うような視線を向けて来ていた。いかんいかん、ぼーっとしていたからルナを心配させてしまっただろうか？この子は怖がりな所もあるし、自分がしっかりしなければとアレンは気を引き締め直した。

「ん？どうした？」
「その……お父さんはどうして、私とリーシャを連れてこの村に暮らすようになったの？お父さんの実力なら、まだ全然冒険者でも通用すると思うんだけど……」

　聞きづらそうな表情を浮かべて指をモジモジと動かしながらルナはそう聞いて来た。単純に子供として気になったのだろう。嬉しい事を言ってくれるが、残念ながら自分はギルドで戦力外通告を言い渡された身なのだ。現実はもっと厳しいという事だな。アレンは苦々しく笑った。

　そしてルナが気にしている事はもっと別の事だろうとアレンは予測する。この村では自分は王都で冒険者をやっている間、リーシャとルナの二人の子供を作り、それで引退して村に戻って来たという事になっている。というかそういう風に解釈されてしまった。アレンもギルドに戦力外通告って言われたというのを伝えるのが恥ずかしかった為、ろくに反論もせずそういう事になってしまったのだ。

　そしてそこで疑問に浮かぶのが母親という存在だろう。一応周りは気を遣って質問しないでくれてるが、実際の所はアレンも母親が誰なのか分からない。そもそも彼は二人の本当の父親ではないのだ。リーシャとルナには必要ないと思ってこの事は伝えていないが、いずれ伝えなければいけないかも知れない。心苦しいが……それでも今はまだ伝えなくて良いはずだ。準備が出来た時、二人が一人前になった時に伝えれば良い。そうしてアレンが面倒な事を後回しにした結果、村の中では彼は王都で色々あって子供二人を抱えて故郷に帰って来た訳ありおっさん、という何とも濃い人物になってしまっているのである。きっとリーシャだって本当は気になっているだろう。自分達の母親は誰なのかを。本当は二人も血が繋がった姉妹ではないのだが……とにかく、ルナはその事についてそれとなく聞き出そうとしているのだ。アレンそう判断した上でどう答えるべきかと自身の髭を弄りながら悩んだ。

「んー、まぁ歳だったのは事実だし、実際仕事に支障が出てたからな……それなら村に戻って畑耕しながらのんびり暮らす方が良いんじゃないかと思って……」

　結局無難にそう答えた。本当はギルドからクビ言い渡されてのこのこ帰って来た駄目おっさんだけど。許してくれルナ。おっさんにもおっさんなりの意地があるんだ。それにこういう答え方が母親の事とかうやむやになりそうだし。とアレンは心の中で謝った。

「じゃぁ……未練はないの？私達と暮らしてる方が、お父さんは楽しいの？」
「当り前だろ、そんなの」

　今度のルナの質問にはアレンはすぐに答えた。ルナ達と暮らしていて楽しいのは本当だ。毎日すくすく成長していく二人を見ているだけで彼は幸せな気持ちになる。これだけは本当だ。そう素直に言うと、ルナはちょっとだけ照れたような表情をしていた。

「そっか……私も、お父さん達と一緒に居るのが一番幸せ」
「ははは、そいつは良かった。さてと……それじゃ授業は終わりだ。そろそろお昼ご飯にするか。リーシャを呼んできてくれ。ルナ」
「うん」

　そろそろ昼飯の時間だと気付き、アレン達はお昼ご飯にする事にした。リーシャを呼びに行ったルナはとてとてと部屋から出て行った。その後ろ姿を見ながらアレンはよっこらせと身体を起こした。




=====================================

感想、ご指摘など頂けましたら幸いです。


－－－－－－－－－－－－－－－END

